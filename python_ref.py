"""
Ian QImplements: "Fast k-Nearest Neighbor Search via Prioritized DCI"
Paper link: https://arxiv.org/pdf/1703.00440.pdf- Verified against brute-force search on sklearn make_blobs dataset- At various points, I'll add NOTE which quotes parts of the paper since the algorithm in the paper isn't very clear
at certain points- Based VERY loosely off https://github.com/UOMXiaoShuaiShuai/PDCI/blob/master/Python/pdci.pyTODO:
    1) test construction time against all of monetizable. (1 hour to construct indices, then 1.5 hours on every query but we can probably parallelize it). Also, this is on scheduler
    2) Then, implement speedups as I'm sure in some places there are inefficiences
    3) implement dynamic updating (we shouldn't need to reconstruct the DS every time)"""

import heapq
import time
from itertools import islice, chain
from queue import PriorityQueue
from typing import Tupleimport numpy as np
from sklearn.datasets.samples_generator import make_blobs
from sortedcontainers import SortedDict
import copy
np.random.seed(420)

class TopK(object):
    """
    We're iterating through a ton of points. So, we should just store the K largest to avoid wasting space
    """
    def __init__(self, max_size):
        self.distances = []
        self.max_size = max_size

    def insert(self, dist, ind):
        dist *= -1
        if len(self.distances) < self.max_size:
            heapq.heappush(self.distances, (dist, ind))
        else:
            heapq.heappushpop(self.distances, (dist, ind))def closest_key(sorted_dict: SortedDict, key: float) -> Tuple:
    """
    Return Tuple of (closest_key, sorted_dict[closest_key)]
    closest key in `sorted_dict` to given `key`.
    https://stackoverflow.com/a/22997000/3532564
    """
    # WARNING: if it errors out here, it is likely that you reduced k0 or k1 which means you're not visiting enough
    # points, thus our sorted_dict doesn't get filled up enough
    keys = list(chain(
        islice(sorted_dict.irange(minimum=key), 1),
        islice(sorted_dict.irange(maximum=key, reverse=True), 1)
    ))    _key = min(keys, key=lambda k: abs(key - k))    # Our options are to pop the old key such that the next key is the smallest, but this takes a decent amount of time
    # So, we just index    return _key, sorted_dict.pop(_key)def construct_data_structure(dataset: np.ndarray, simple_indices: int, composite_indices: int):

    Construct the data structure according to the algorithm in the paper    :param dataset: whatever dataset we want to construct the thing of
    :param simple_indices:
    :param composite_indices:
    :return:
        random projections, data structure that imposes ordering
    """
    dims = dataset.shape[1]    # Pre-allocate the data structures so we don't get slowed down
    uvecs = np.empty((simple_indices, composite_indices, dims), object)
    ordering_structure = np.empty((simple_indices, composite_indices), object)
    random_projections = np.random.normal(0, 1, (simple_indices, composite_indices, dims))    for j in range(simple_indices):
        for l in range(composite_indices):
            v = random_projections[j, l]
            uvec = v / np.linalg.norm(v)            # Progressively store the unit-vectored random projection
            uvecs[j, l] = uvec
            projs = np.dot(dataset, uvec)            """
            NOTE: "implemented using standard data structures that maintain one-d ordered sequences of elements"
            - Page 4, right above Algorithm 1            # Here, we use a SortedDict which is as fast as native C code (!!!) and implements a 1-D ordering
            """
            ordering_structure[j, l] = SortedDict()
            for i, el in enumerate(projs): ordering_structure[j, l][el] = i    return uvecs, ordering_structuredef query_data_structure(query_pt, uvecs, ordering_data_struct, dataset, k0, k1, num_nearest_neighbors, tol:float=1.0):
        """
        For a query_pt, check against the random projections and then find the closest point(s) in the dataset to it    :param query_pt:
            Our query point
        :param uvecs:
            Unit-vectors generated
        :param ordering_data_struct_cp:
            a data structure that imposes a 1-D ordering on the projected distances
        :param dataset:
            our dataset
        :param k0:
            proof-derived bound on number of neighbors to visit
        :param k1:
            proof-derived bound on number of points in composite indices to visit
        :param num_nearest_neighbors:
            K in a standard K-NN problem
        :param tol:
            tolerance is a float multiplied by the m, the number of simple indices. In the paper, they require that the count
            is equal to m before considering it a candidate but we can relax that in lieu of relaxing k0 and k1
        :return:
        """
        ordering_data_struct_cp = copy.deepcopy(ordering_data_struct)  # We iterate and consume it. Might not be what we want in the long run
        # Construct all the holders and structures set out in the paper
        simple_indices_m = uvecs.shape[0]
        complex_indices_L = uvecs.shape[1]
        count_array_C = np.zeros((complex_indices_L, dataset.shape[0]))  # Stored as (L, N) as that's what the notation implies even though it reads funny
        candidate_sets_S = [set() for _ in range(complex_indices_L)]
        priority_qs_P = [[] for _ in range(complex_indices_L)]
        q_projs = np.dot(uvecs, query_pt)    """
    NOTE: P-DCI assigns a priority to each constituent simple index; in each iteration, it visits the upcoming point from 
    the simple index with the highest priority and updates the priority at the end of the iteration.    Ian: NOTE: "The priority of a simple index is set to the negative absolute difference between...."
        - we don't need to set to -abs() because our data structure already handles it for us. Fuck yeah PQs    - Page 4 Above Algorithm 2
    """    # Seed the priority queue according to the algorithm
        for l in range(complex_indices_L):
            for j in range(simple_indices_m):
                p_proj, pt = closest_key(ordering_data_struct_cp[j, l], q_projs[j, l])
                priority = abs(p_proj - q_projs[j, l])
                heapq.heappush(priority_qs_P[l], (priority, pt, j, l))    # Then, go through the random projections
        for i in range(k1):
            for l in range(complex_indices_L):
                if len(candidate_sets_S[l]) < k0 and priority_qs_P[l]:
                    _, point, origin_j, origin_l = heapq.heappop(priority_qs_P[l])
                    # No entry in the ordering -> have exhausted all potential points
                    ordering_struct_is_exhausted = len(ordering_data_struct_cp[origin_j, origin_l]) <= 0
                    # # Quick way to short circuit since we'd just be redundantly adding it back
                    count_is_saturated = count_array_C[l, point] == int(simple_indices_m * tol)
                    if ordering_struct_is_exhausted or count_is_saturated:
                        continue                p_proj, pt = closest_key(ordering_data_struct_cp[origin_j, origin_l], q_projs[origin_j, origin_l])                priority = abs(p_proj - q_projs[origin_j, origin_l])  #
                    heapq.heappush(priority_qs_P[l], (priority, pt, origin_j, origin_l))                """
                NOTE: ... keeps track of how many times each data point has been visited across all simple indices of 
                each composite index                - Page 4 above Algorithm 1
                """
                    count_array_C[l, point] += 1                """
                NOTE: If a data point has been visited in every constituent simple index, it is added to the candidate 
                set                - Page 4 above Algorithm 1
                """
                    if count_array_C[l, point] == int(simple_indices_m * tol):
                        candidate_sets_S[l].add(point)    # Finally, take the union of all the candidates, then store the Top-K and treat it as a streaming problem
        # Obviously, we don't need all of it in memory so no sense in doing such
        candidates = set()
        for l in range(complex_indices_L):
            candidates = candidates.union(candidate_sets_S[l])
        keep_top_k_distances = TopK(max_size=num_nearest_neighbors)
        for point in candidates:
            dist = np.linalg.norm(query_pt - dataset[point])
            keep_top_k_distances.insert(dist=dist, ind=point)
        closest_k_arr = np.asarray(keep_top_k_distances.distances)[:, 1]
        return closest_k_arr